/*****
* CONFIGURATION
*/
    //Main navigation
    $.navigation = $('nav > ul.nav');

  $.panelIconOpened = 'icon-arrow-up';
  $.panelIconClosed = 'icon-arrow-down';

  //Default colours
  $.brandPrimary =  '#20a8d8';
  $.brandSuccess =  '#4dbd74';
  $.brandInfo =     '#63c2de';
  $.brandWarning =  '#f8cb00';
  $.brandDanger =   '#f86c6b';

  $.grayDark =      '#2a2c36';
  $.gray =          '#55595c';
  $.grayLight =     '#818a91';
  $.grayLighter =   '#d1d4d7';
  $.grayLightest =  '#f8f9fa';

'use strict';

/****
* MAIN NAVIGATION
*/

$(document).ready(function($){

  // Add class .active to current link
  $.navigation.find('a').each(function(){

    var cUrl = String(window.location).split('?')[0];

    if (cUrl.substr(cUrl.length - 1) == '#') {
      cUrl = cUrl.slice(0,-1);
    }

    if ($($(this))[0].href==cUrl) {
      $(this).addClass('active');

      $(this).parents('ul').add(this).each(function(){
        $(this).parent().addClass('open');
      });
    }
  });

  // Dropdown Menu
  $.navigation.on('click', 'a', function(e){

    if ($.ajaxLoad) {
      e.preventDefault();
    }

    if ($(this).hasClass('nav-dropdown-toggle')) {
      $(this).parent().toggleClass('open');
      resizeBroadcast();
    }

  });

  function resizeBroadcast() {

    var timesRun = 0;
    var interval = setInterval(function(){
      timesRun += 1;
      if(timesRun === 5){
        clearInterval(interval);
      }
      window.dispatchEvent(new Event('resize'));
    }, 62.5);
  }

  /* ---------- Main Menu Open/Close, Min/Full ---------- */
  $('.sidebar-toggler').click(function(){
    $('body').toggleClass('sidebar-hidden');
    resizeBroadcast();
  });

  $('.sidebar-minimizer').click(function(){
    $('body').toggleClass('sidebar-minimized');
    resizeBroadcast();
  });

  $('.brand-minimizer').click(function(){
    $('body').toggleClass('brand-minimized');
  });

  $('.aside-menu-toggler').click(function(){
    $('body').toggleClass('aside-menu-hidden');
    resizeBroadcast();
  });

  $('.mobile-sidebar-toggler').click(function(){
    $('body').toggleClass('sidebar-mobile-show');
    resizeBroadcast();
  });

  $('.sidebar-close').click(function(){
    $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
  });

  /* ---------- Disable moving to top ---------- */
  $('a[href="#"][data-top!=true]').click(function(e){
    e.preventDefault();
  });

});

/****
* CARDS ACTIONS
*/

$(document).on('click', '.card-actions a', function(e){
  e.preventDefault();

  if ($(this).hasClass('btn-close')) {
    $(this).parent().parent().parent().fadeOut();
  } else if ($(this).hasClass('btn-minimize')) {
    var $target = $(this).parent().parent().next('.card-block');
    if (!$(this).hasClass('collapsed')) {
      $('i',$(this)).removeClass($.panelIconOpened).addClass($.panelIconClosed);
    } else {
      $('i',$(this)).removeClass($.panelIconClosed).addClass($.panelIconOpened);
    }

  } else if ($(this).hasClass('btn-setting')) {
    $('#myModal').modal('show');
  }

});

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function init(url) {

  /* ---------- Tooltip ---------- */
  $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"bottom",delay: { show: 400, hide: 200 }});

  /* ---------- Popover ---------- */
  $('[rel="popover"],[data-rel="popover"],[data-toggle="popover"]').popover();

}


/****
 * ATTACKERS OVER TIME
 * */



var list = d3.select('#martin'),
    country_list = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","England","Fiji","France","Italy","Zimbabwe"],
    dayMonthYearFormat = d3.time.format("%d %b %Y"),
    randomImage = (function() {

      var r = Math.random();

      return '<img src="img/flags/' + country_list[Math.floor(r * country_list.length)] + '.png"/>'
    });

/**
 * PIE
 *
 */
var w = 200,
    h = 200,
    r = 100,
    aspect = w / h
    color = d3.scale.category20c();

var arc = d3.svg.arc()
    .outerRadius(r - 20)
    .innerRadius(r - 50);

var pie = d3.layout.pie()
    .sort(null)
    .value(function(d) {
        return d.values;
    });

var pieAttackerSvg = d3.select("#pieAttacker").append("svg")
     .attr("width", w)
     .attr("height", h)
    .append("g")
    .attr("transform", "translate(" + w / 2 + "," + h / 2 + ")");

var pieCanarySvg = d3.select("#pieCanary").append("svg")
    .attr("width", w)
    .attr("height", h)
    .append("g")
    .attr("transform", "translate(" + w / 2 + "," + h / 2 + ")");

var pieTypesSvg = d3.select("#pieTypes").append("svg")
    .attr("width", w)
    .attr("height", h)
    .append("g")
    .attr("transform", "translate(" + w / 2 + "," + h / 2 + ")");

var tooltip = d3.select('#pieAttacker')                               
    .append('div')                                                
    .attr('class', 'pie-tooltip');
tooltip.append('div')
    .attr('class', 'color-box');

tooltip.append('div')                                           
    .attr('class', 'label');

var tooltipC = d3.select('#pieCanary')
    .append('div')
    .attr('class', 'pie-tooltip');
tooltipC.append('div')
    .attr('class', 'color-box');

tooltipC.append('div')
    .attr('class', 'label');

var tooltipT = d3.select('#pieTypes')
    .append('div')
    .attr('class', 'pie-tooltip');
tooltipT.append('div')
    .attr('class', 'color-box');

tooltipT.append('div')
    .attr('class', 'label');


d3.json("data/data.json", function(error,json) {
    if (error) throw error;

    var attackers = d3.nest()
        .key(function(d) { return d.src_host; })
        .entries(json['alerts']);

    var attackersCount = d3.nest()
        .key(function(d) { return d.src_host; })
        .rollup(function(values) {
            return values.length;
        })
        .entries(json['alerts']);

    var canaryCount = d3.nest()
        .key(function(d) { return d.dst_host; })
        .rollup(function(values) {
            return values.length;
        })
        .entries(json['alerts']);

    var typesCount = d3.nest()
        .key(function(d) { return d.description; })
        .rollup(function(values) {
            return values.length;
        })
        .entries(json['alerts']);

    /**
     * List Based D3JS
     */

    var attackersUL = d3.select("#attackers").attr('class','list-group'),
        attackerLI = attackersUL.selectAll("li")
          .data(attackers)
          .enter()
          .append("li")
          .attr("class", function(d,i) {
              var c = "attacker list-group-item remove-border ";
              if (attackersCount[i].values == 1)
              {
                  c += "info";
              }
              if (attackersCount[i].values > 1 && attackersCount[i].values <= 10)
              {
                  c += "warning ";
              }
              if (attackersCount[i].values > 10 )
              {
                  c += "danger";
              }

              return c;
          })
          .html(function(d,i) {

              if (d.key !== '') return  '<span>Attacker : ' + d.key + '</span>' +
                  '<img src="img/flags/' + country_list[Math.floor(Math.random() * country_list.length)] + '.png"/>'
                  + '<i class="fa fa-chevron-down" aria-hidden="true"></i>'
                  + '<span class="attack-counter">' + attackersCount[i].values + '</span>';
              if (d.key === '') return  '<span>Disconnects </span>' +
                  '<img src="img/flags/' + country_list[Math.floor(Math.random() * country_list.length)] + '.png"/>'
                  + '<i class="fa fa-chevron-down" aria-hidden="true"></i>'
                  + '<span class="attack-counter">' + attackersCount[i].values + '</span>';
          })
          .on("click", function(){
              $(this).toggleClass('remove-border');
              $(this).find('.fa').toggle('fa fa-chevron-down');
              $(this).find('.attacks').slideToggle(1000);
              //d3.select(this).selectAll('.attacks').style("display",display);
          });

    var attackerULDetails = attackerLI.append("ul")
        .on("click", function(){
            d3.event.preventDefault();
        })
        .attr("class", "attacks")
        .selectAll("li")
        .data(function(d) {return d.values;})

        .enter()
        .append("li")
        .attr('class','attack-details')
        .html(function(d) {
          return '<span class="small text-muted">Canary:</span>'
              + '<span class="small text-muted">Type:</span>'
              + '<span class="small text-muted">Time:</span>'
              + '<strong>' + d.dst_host + '</strong>'
              + '<strong>' + d.description + '</strong>'
              + '<strong>' + dayMonthYearFormat(new Date(d.created * 1000)) + '</strong>'});


    /**
     *  Pie Chart D3JS
     */

    //////
    var g = pieAttackerSvg.selectAll(".arc")
        .data(pie(attackersCount))
        .enter().append("g")
        .attr("class", "arc");

    g.append("text")
        .attr("transform", function(d) {
            return "translate(" + arc.centroid(d) + ")";
        })
        .attr("dy", ".35em")
        .style("text-anchor", "middle");

    g.append("path")
        .attr("d", arc)
        .attr("fill", function(d, i) { return color(i); } )
        .on('mouseover', function(d,i) {
            tooltip.select('.label').html(d.data.key);
            tooltip.style('display', 'inline-block')
            tooltip.select('.color-box').style('background', color(i))
        });

    var gC = pieCanarySvg.selectAll(".arc")
        .data(pie(canaryCount))
        .enter().append("g")
        .attr("class", "arc");

    gC.append("path")
        .attr("d", arc)
        .attr("fill", function(d, i) { return color(i); } )
        .on('mouseover', function(d,i) {
            tooltipC.select('.label').html(d.data.key);
            tooltipC.style('display', 'inline-block')
            tooltipC.select('.color-box').style('background', color(i))
        });

    gC.append("text")
        .attr("transform", function(d) {
            return "translate(" + arc.centroid(d) + ")";
        })
        .attr("dy", ".35em")
        .style("text-anchor", "middle");

    var gT = pieTypesSvg.selectAll(".arc")
        .data(pie(typesCount))
        .enter().append("g")
        .attr("class", "arc");

    gT.append("path")
        .attr("d", arc)
        .attr("fill", function(d, i) { return color(i); } )
        .on('mouseover', function(d,i) {
            tooltipT.select('.label').html(d.data.key);
            tooltipT.style('display', 'inline-block')
            tooltipT.select('.color-box').style('background', color(i))
        });

    gT.append("text")
        .attr("transform", function(d) {
            return "translate(" + arc.centroid(d) + ")";
        })
        .attr("dy", ".35em")
        .style("text-anchor", "middle");
});


